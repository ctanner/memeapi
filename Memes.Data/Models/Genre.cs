﻿using System.ComponentModel.DataAnnotations;

namespace Memes.Data.Models
{
    public class Genre : IId
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int SortOrder { get; set; }
    }
}
