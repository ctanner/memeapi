﻿using System.ComponentModel.DataAnnotations;

namespace Memes.Data.Models
{
    public class Image : IId
    {
        public int Id { get; set; }

        [Required, MaxLength(128)]
        public string Name { get; set; }

        [Required]
        public string Url { get; set; }

        [MaxLength(1024)]
        public string Description { get; set; }

        public int GenreId { get; set; }
        public Genre Genre { get; set; }
    }
}
