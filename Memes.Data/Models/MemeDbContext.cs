﻿using System.Data.Entity;

namespace Memes.Data.Models
{
    public class MemeDbContext : DbContext
    {
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Image> Images { get; set; }

        public MemeDbContext() 
        {
            //Database.SetInitializer(new MemeDbInitializer());
            Database.SetInitializer<MemeDbContext>(null);

            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }
    }

    public class MemeDbInitializer : DropCreateDatabaseAlways<MemeDbContext>
    {
        protected override void Seed(MemeDbContext context)
        {
            context.Genres.AddRange(new[]
            {
                new Genre { Name = "Cats", SortOrder = 1 },
                new Genre { Name = "Dogs", SortOrder = 2 },
            });

            context.Images.AddRange(new[]
            {
                new Image { Name = "Cat 1", Url = "https://cdn.meme.am/cache/instances/folder873/250x250/34362873.jpg", Description = "A cat", GenreId = 1 },
                new Image { Name = "Cat 2", Url = "https://cdn.meme.am/cache/instances/folder376/250x250/34620376.jpg", Description = "A cat", GenreId = 1 },
                new Image { Name = "Dog 1", Url = "https://cdn.meme.am/cache/instances/folder165/400x400/20135165.jpg", Description = "A dog", GenreId = 2 },
            });
            
            base.Seed(context);
        }
    }
}
