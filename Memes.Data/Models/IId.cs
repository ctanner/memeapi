﻿namespace Memes.Data.Models
{
    public interface IId
    {
        int Id { get; set; }
    }
}
