﻿using Memes.Data.Models;

namespace Memes.Data.Repositories
{
    public interface IGenreRepository : IRepository<Genre>
    {
    }

    public class GenreRepository : GenericRepository<Genre>, IGenreRepository
    {
        public GenreRepository(MemeDbContext dbContext) : base(dbContext)
        {
        }
    }
}
