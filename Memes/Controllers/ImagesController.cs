﻿using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using Memes.Data.Models;
using Memes.Data.Repositories;
using System.Linq;
using System.Web.Http.Cors;

namespace Memes.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class ImagesController : ApiController
    {
        private IImageRepository _imageRepo;

        public ImagesController(IImageRepository imageRepo)
        {
            _imageRepo = imageRepo;
        }

        // GET: api/Images
        public IQueryable<Image> GetImages(int? genreId = null)
        {
            if (genreId.HasValue)
                return _imageRepo.GetByGenre(genreId.Value);

            return _imageRepo.GetAll();
        }

        // GET: api/Images/5
        [ResponseType(typeof(Image))]
        public IHttpActionResult GetImage(int id)
        {
            Image image = _imageRepo.GetById(id);

            if (image == null)
            {
                return NotFound();
            }

            return Ok(image);
        }

        // PUT: api/Images/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutImage(int id, Image image)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != image.Id)
            {
                return BadRequest();
            }

            if(!_imageRepo.Exists(id))
            {
                return NotFound();
            }

            _imageRepo.Update(image);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Images
        [ResponseType(typeof(Image))]
        public IHttpActionResult PostImage(Image image)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _imageRepo.Add(image);
            
            return CreatedAtRoute("DefaultApi", new { id = image.Id }, image);
        }

        // DELETE: api/Images/5
        [ResponseType(typeof(Image))]
        public IHttpActionResult DeleteImage(int id)
        {
            Image image = _imageRepo.GetById(id);

            if (image == null)
            {
                return NotFound();
            }

            _imageRepo.Delete(image);
            
            return Ok(image);
        }
    }
}