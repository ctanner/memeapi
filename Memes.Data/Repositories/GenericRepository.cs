﻿using Memes.Data.Models;
using System.Data.Entity;
using System.Linq;

namespace Memes.Data.Repositories
{
    public class GenericRepository<T> where T : class, IId
    {
        protected DbSet<T> _dbSet;
        protected MemeDbContext _dbContext;

        public GenericRepository(MemeDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<T>();
        }

        public virtual void Add(T entity)
        {
            _dbSet.Add(entity);
            _dbContext.SaveChanges();
        }

        public virtual void Update(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            _dbContext.SaveChanges();
        }

        public virtual void Delete(T entity)
        {
            _dbSet.Remove(entity);
            _dbContext.SaveChanges();
        }

        public virtual IQueryable<T> GetAll()
        {
            return _dbSet.AsQueryable();
        }

        public virtual T GetById(int id)
        {
            return _dbSet.Find(id);
        }

        public virtual bool Exists(int id)
        {
            return _dbSet.Count(e => e.Id == id) > 0;
        }
    }
}
