﻿using Memes.Data.Models;
using System.Linq;

namespace Memes.Data.Repositories
{
    public interface IImageRepository : IRepository<Image>
    {
        IQueryable<Image> GetByGenre(int genreId);
    }

    public class ImageRepository : GenericRepository<Image>, IImageRepository
    {
        public ImageRepository(MemeDbContext dbContext) : base(dbContext)
        {
        }

        public override IQueryable<Image> GetAll()
        {
            return _dbSet.Include("Genre").AsQueryable();
        }

        public IQueryable<Image> GetByGenre(int genreId)
        {
            return GetAll().Where(img => img.GenreId == genreId);
        }
    }
}
