webpackJsonp([0,3],{

/***/ 161:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__(771);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return MemeCatalogService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TEST_MEMES = [
    {
        id: 1,
        name: 'One',
        description: 'The first meme',
        url: 'https://cdn.meme.am/cache/instances/folder580/400x400/74407580.jpg',
        genreId: 0
    },
    {
        id: 2,
        name: 'Two',
        description: 'The second meme',
        url: 'https://cdn.meme.am/cache/instances/folder450/250x250/74378450.jpg',
        genreId: 0
    },
    {
        id: 3,
        name: 'Three',
        description: 'The third meme',
        url: 'https://cdn.meme.am/cache/instances/folder736/250x250/74352736.jpg',
        genreId: 1
    },
    {
        id: 4,
        name: 'Four',
        description: 'The fourth meme',
        url: 'https://cdn.meme.am/cache/instances/folder437/250x250/74379437.jpg',
        genreId: 1
    },
    {
        id: 5,
        name: 'Five',
        description: 'The fifth meme',
        url: 'https://cdn.meme.am/cache/instances/folder619/250x250/74388619.jpg',
        genreId: 2
    },
    {
        id: 6,
        name: 'Six',
        description: 'The sixth meme',
        url: 'https://cdn.meme.am/cache/instances/folder580/400x400/74407580.jpg',
        genreId: 0
    },
    {
        id: 7,
        name: 'Seven',
        description: 'The seventh meme',
        url: 'https://cdn.meme.am/cache/instances/folder450/250x250/74378450.jpg',
        genreId: 0
    },
    {
        id: 8,
        name: 'Eight',
        description: 'The eigth meme',
        url: 'https://cdn.meme.am/cache/instances/folder736/250x250/74352736.jpg',
        genreId: 1
    },
    {
        id: 9,
        name: 'Nine',
        description: 'The ninth meme',
        url: 'https://cdn.meme.am/cache/instances/folder437/250x250/74379437.jpg',
        genreId: 1
    },
    {
        id: 10,
        name: 'Ten',
        description: 'The tenth meme',
        url: 'https://cdn.meme.am/cache/instances/folder619/250x250/74388619.jpg',
        genreId: 2
    },
];
var TEST_GENRES = [
    {
        id: 1,
        name: 'Genre 1'
    },
    {
        id: 2,
        name: 'Genre 2'
    },
];
var REQUEST_OPTIONS = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Headers */]({
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    })
});
var MemeCatalogService = (function () {
    function MemeCatalogService(http) {
        this.http = http;
    }
    MemeCatalogService.prototype.getGenres = function () {
        return this.http
            .get('http://localhost:49639/api/genres')
            .toPromise()
            .then(function (res) { return res.json(); });
    };
    MemeCatalogService.prototype.getMemes = function () {
        return this.http
            .get('http://localhost:49639/api/images')
            .toPromise()
            .then(function (res) { return res.json(); });
    };
    MemeCatalogService.prototype.getMemesByGenre = function (genreId) {
        return this.http
            .get("http://localhost:49639/api/images/?genreId=" + genreId)
            .toPromise()
            .then(function (res) { return res.json(); });
    };
    MemeCatalogService.prototype.addMeme = function (meme) {
        return this.http
            .post('http://localhost:49639/api/images', JSON.stringify(meme), REQUEST_OPTIONS)
            .toPromise()
            .then(function (res) { return res.json(); });
    };
    MemeCatalogService.prototype.removeMeme = function (memeId) {
        return this.http
            .delete("http://localhost:49639/api/images/" + memeId, REQUEST_OPTIONS)
            .toPromise()
            .then(function (res) { return res.json(); });
    };
    MemeCatalogService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["R" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === 'function' && _a) || Object])
    ], MemeCatalogService);
    return MemeCatalogService;
    var _a;
}());
//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/meme-catalog.service.js.map

/***/ },

/***/ 393:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__meme_catalog_service__ = __webpack_require__(161);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return AddMemeDialogComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddMemeDialogComponent = (function () {
    function AddMemeDialogComponent(dialogRef, catalogService) {
        this.dialogRef = dialogRef;
        this.catalogService = catalogService;
    }
    AddMemeDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.catalogService.getGenres().then(function (genres) { return _this.genres = genres; });
        this.meme = { name: null, description: null, url: null, genreId: null };
    };
    AddMemeDialogComponent.prototype.onSubmit = function () {
        this.dialogRef.close(this.meme);
    };
    AddMemeDialogComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* Component */])({
            selector: 'add-meme-dialog',
            template: __webpack_require__(757),
            styles: [__webpack_require__(752)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_material__["MdDialogRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_material__["MdDialogRef"]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__meme_catalog_service__["a" /* MemeCatalogService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__meme_catalog_service__["a" /* MemeCatalogService */]) === 'function' && _b) || Object])
    ], AddMemeDialogComponent);
    return AddMemeDialogComponent;
    var _a, _b;
}());
//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/add-meme-dialog.component.js.map

/***/ },

/***/ 394:
/***/ function(module, exports) {

;
//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/meme.js.map

/***/ },

/***/ 395:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models__ = __webpack_require__(597);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__meme_catalog_service__ = __webpack_require__(161);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return RemoveMemeDialogComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RemoveMemeDialogComponent = (function () {
    function RemoveMemeDialogComponent(dialogRef, catalogService) {
        this.dialogRef = dialogRef;
        this.catalogService = catalogService;
    }
    RemoveMemeDialogComponent.prototype.ngOnInit = function () {
    };
    RemoveMemeDialogComponent.prototype.cancel = function () {
        this.dialogRef.close(false);
    };
    RemoveMemeDialogComponent.prototype.confirm = function () {
        this.dialogRef.close(true);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Input */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__models__["Meme"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__models__["Meme"]) === 'function' && _a) || Object)
    ], RemoveMemeDialogComponent.prototype, "meme", void 0);
    RemoveMemeDialogComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* Component */])({
            selector: 'remove-meme-dialog',
            template: __webpack_require__(761),
            styles: [__webpack_require__(756)]
        }), 
        __metadata('design:paramtypes', [(typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_material__["MdDialogRef"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_material__["MdDialogRef"]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__meme_catalog_service__["a" /* MemeCatalogService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__meme_catalog_service__["a" /* MemeCatalogService */]) === 'function' && _c) || Object])
    ], RemoveMemeDialogComponent);
    return RemoveMemeDialogComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/remove-meme-dialog.component.js.map

/***/ },

/***/ 443:
/***/ function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 443;


/***/ },

/***/ 444:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__polyfills_ts__ = __webpack_require__(599);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__polyfills_ts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__polyfills_ts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(572);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(598);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_module__ = __webpack_require__(593);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_37" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_4__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/main.js.map

/***/ },

/***/ 592:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__(758),
            styles: [__webpack_require__(753)]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/app.component.js.map

/***/ },

/***/ 593:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_material__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__meme_catalog_service__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(592);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__meme_catalog_meme_catalog_component__ = __webpack_require__(595);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__meme_card_meme_card_component__ = __webpack_require__(594);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__add_meme_add_meme_dialog_component__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__remove_meme_remove_meme_dialog_component__ = __webpack_require__(395);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_7__meme_catalog_meme_catalog_component__["a" /* MemeCatalogComponent */],
                __WEBPACK_IMPORTED_MODULE_8__meme_card_meme_card_component__["a" /* MemeCardComponent */],
                __WEBPACK_IMPORTED_MODULE_9__add_meme_add_meme_dialog_component__["a" /* AddMemeDialogComponent */],
                __WEBPACK_IMPORTED_MODULE_10__remove_meme_remove_meme_dialog_component__["a" /* RemoveMemeDialogComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_material__["MaterialModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["e" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* HttpModule */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_5__meme_catalog_service__["a" /* MemeCatalogService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* AppComponent */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_9__add_meme_add_meme_dialog_component__["a" /* AddMemeDialogComponent */],
                __WEBPACK_IMPORTED_MODULE_10__remove_meme_remove_meme_dialog_component__["a" /* RemoveMemeDialogComponent */]
            ]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/app.module.js.map

/***/ },

/***/ 594:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_meme__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_meme___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__models_meme__);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return MemeCardComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MemeCardComponent = (function () {
    function MemeCardComponent() {
        this.remove = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["_20" /* EventEmitter */]();
        this.share = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["_20" /* EventEmitter */]();
    }
    MemeCardComponent.prototype.ngOnInit = function () {
    };
    MemeCardComponent.prototype.onRemove = function () {
        this.remove.emit(this.meme);
    };
    MemeCardComponent.prototype.onShare = function () {
        this.share.emit(this.meme.url);
    };
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Input */])(), 
        __metadata('design:type', (typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_meme__["Meme"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__models_meme__["Meme"]) === 'function' && _a) || Object)
    ], MemeCardComponent.prototype, "meme", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Input */])(), 
        __metadata('design:type', String)
    ], MemeCardComponent.prototype, "genreName", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Output */])(), 
        __metadata('design:type', Object)
    ], MemeCardComponent.prototype, "remove", void 0);
    __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Output */])(), 
        __metadata('design:type', Object)
    ], MemeCardComponent.prototype, "share", void 0);
    MemeCardComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* Component */])({
            selector: 'meme-card',
            template: __webpack_require__(759),
            styles: [__webpack_require__(754)]
        }), 
        __metadata('design:paramtypes', [])
    ], MemeCardComponent);
    return MemeCardComponent;
    var _a;
}());
//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/meme-card.component.js.map

/***/ },

/***/ 595:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__meme_catalog_service__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__add_meme_add_meme_dialog_component__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__remove_meme_remove_meme_dialog_component__ = __webpack_require__(395);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return MemeCatalogComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MemeCatalogComponent = (function () {
    function MemeCatalogComponent(catalogService, dialog) {
        this.catalogService = catalogService;
        this.dialog = dialog;
    }
    MemeCatalogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.catalogService.getGenres().then(function (genres) { return _this.genres = genres; });
        this.showAll();
    };
    MemeCatalogComponent.prototype.showAll = function () {
        var _this = this;
        this.catalogService.getMemes().then(function (memes) { return _this.memes = memes; });
    };
    MemeCatalogComponent.prototype.showForGenre = function (genreId) {
        var _this = this;
        this.catalogService.getMemesByGenre(genreId).then(function (memes) { return _this.memes = memes; });
    };
    MemeCatalogComponent.prototype.openAddDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_3__add_meme_add_meme_dialog_component__["a" /* AddMemeDialogComponent */]);
        dialogRef.afterClosed().subscribe(function (result) {
            if (typeof result !== 'undefined') {
                _this.catalogService.addMeme(result).then(function (meme) { return _this.memes.push(meme); });
            }
        });
    };
    MemeCatalogComponent.prototype.openRemoveDialog = function (meme) {
        var _this = this;
        var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_4__remove_meme_remove_meme_dialog_component__["a" /* RemoveMemeDialogComponent */]);
        dialogRef.componentInstance.meme = meme;
        dialogRef.afterClosed().subscribe(function (confirmed) {
            if (confirmed === true) {
                _this.catalogService.removeMeme(meme.id).then(function (_) {
                    _this.memes = _this.memes.filter(function (m) { return m.id !== meme.id; });
                });
            }
        });
    };
    MemeCatalogComponent.prototype.onShareMeme = function (url) {
        window.prompt('Copy to clipboard: Ctrl+C, Enter', url);
    };
    MemeCatalogComponent.prototype.getGenreName = function (meme) {
        if (this.genres) {
            var genre = this.genres.filter(function (g) { return g.id === meme.genreId; })[0];
            return genre.name;
        }
        return '';
    };
    MemeCatalogComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["G" /* Component */])({
            selector: 'meme-catalog',
            template: __webpack_require__(760),
            styles: [__webpack_require__(755)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__meme_catalog_service__["a" /* MemeCatalogService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__meme_catalog_service__["a" /* MemeCatalogService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_material__["MdDialog"] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_material__["MdDialog"]) === 'function' && _b) || Object])
    ], MemeCatalogComponent);
    return MemeCatalogComponent;
    var _a, _b;
}());
//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/meme-catalog.component.js.map

/***/ },

/***/ 596:
/***/ function(module, exports) {

;
//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/genre.js.map

/***/ },

/***/ 597:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__meme__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__meme___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__meme__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__genre__ = __webpack_require__(596);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__genre___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__genre__);
/* harmony namespace reexport (by used) */ if(__webpack_require__.o(__WEBPACK_IMPORTED_MODULE_0__meme__, "Meme")) __webpack_require__.d(exports, "Meme", function() { return __WEBPACK_IMPORTED_MODULE_0__meme__["Meme"]; });
/* harmony namespace reexport (by used) */ if(__webpack_require__.o(__WEBPACK_IMPORTED_MODULE_1__genre__, "Meme")) __webpack_require__.d(exports, "Meme", function() { return __WEBPACK_IMPORTED_MODULE_1__genre__["Meme"]; });


//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/index.js.map

/***/ },

/***/ 598:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/environment.js.map

/***/ },

/***/ 599:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol__ = __webpack_require__(613);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es6_object__ = __webpack_require__(606);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es6_object___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_core_js_es6_object__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_core_js_es6_function__ = __webpack_require__(602);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_core_js_es6_function___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_core_js_es6_function__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int__ = __webpack_require__(608);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float__ = __webpack_require__(607);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_core_js_es6_number__ = __webpack_require__(605);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_core_js_es6_number___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_core_js_es6_number__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_core_js_es6_math__ = __webpack_require__(604);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_core_js_es6_math___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_core_js_es6_math__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_core_js_es6_string__ = __webpack_require__(612);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_core_js_es6_string___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_core_js_es6_string__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_core_js_es6_date__ = __webpack_require__(601);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_core_js_es6_date___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_core_js_es6_date__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_core_js_es6_array__ = __webpack_require__(600);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_core_js_es6_array___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_core_js_es6_array__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp__ = __webpack_require__(610);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_core_js_es6_map__ = __webpack_require__(603);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_core_js_es6_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_core_js_es6_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_core_js_es6_set__ = __webpack_require__(611);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_core_js_es6_set___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_core_js_es6_set__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect__ = __webpack_require__(609);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect__ = __webpack_require__(614);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone__ = __webpack_require__(797);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone__);
















//# sourceMappingURL=C:/Users/ctanner/Documents/memes/src/polyfills.js.map

/***/ },

/***/ 752:
/***/ function(module, exports) {

module.exports = ".example-full-width {\r\n  width: 100%;\r\n}"

/***/ },

/***/ 753:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 754:
/***/ function(module, exports) {

module.exports = ".meme-card {\r\n  max-width: 200px;\r\n}\r\n"

/***/ },

/***/ 755:
/***/ function(module, exports) {

module.exports = ".navbar {\r\n    margin-bottom: 16px;\r\n}\r\n\r\n.fill-space {\r\n  -webkit-box-flex: 1;\r\n      -ms-flex: 1 1 auto;\r\n          flex: 1 1 auto;\r\n}\r\n\r\nmd-select .md-select-placeholder,\r\nmd-select .md-select-value,\r\nmd-select .md-select-arrow {\r\n    color: lightgray !important;\r\n}\r\n"

/***/ },

/***/ 756:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 757:
/***/ function(module, exports) {

module.exports = "<form (ngSubmit)=\"onSubmit()\" #memeForm=\"ngForm\" class=\"example-form\">\r\n    <h1 md-dialog-title>Dialog</h1>\r\n    \r\n    <div md-dialog-content>\r\n        <md-input-container class=\"example-full-width\">\r\n            <input md-input id=\"name\" name=\"name\" [(ngModel)]=\"meme.name\" placeholder=\"Name\" required>\r\n        </md-input-container>\r\n\r\n        <md-input-container class=\"example-full-width\">\r\n            <input md-input id=\"url\" name=\"url\" [(ngModel)]=\"meme.url\" placeholder=\"URL\" required>\r\n        </md-input-container>\r\n    \r\n        <md-input-container class=\"example-full-width\">\r\n            <textarea md-input id=\"description\" name=\"description\" [(ngModel)]=\"meme.description\" placeholder=\"Description\"></textarea>\r\n        </md-input-container>\r\n\r\n        <div style=\"margin-top: 16px;\">\r\n            <md-select id=\"genre\" name=\"genre\" [(ngModel)]=\"meme.genreId\" placeholder=\"Genre\" required>\r\n                <md-option *ngFor=\"let genre of genres\" [value]=\"genre.id\">\r\n                    {{genre.name}}\r\n                </md-option>\r\n            </md-select>\r\n        </div>\r\n    </div>\r\n\r\n    <div md-dialog-actions>\r\n        <button type=\"button\" md-button md-dialog-close>Cancel</button>\r\n        <button type=\"submit\" md-button [disabled]=\"!memeForm.form.valid\">Save</button>\r\n    </div>\r\n</form>\r\n\r\n"

/***/ },

/***/ 758:
/***/ function(module, exports) {

module.exports = "<meme-catalog></meme-catalog>\n\n\n"

/***/ },

/***/ 759:
/***/ function(module, exports) {

module.exports = "<md-card class=\"meme-card\">\n  <md-card-header>\n    <md-card-title>{{meme.name}}</md-card-title>\n    <md-card-subtitle>{{genreName}}</md-card-subtitle>\n  </md-card-header>\n  <img md-card-image src=\"{{meme.url}}\">\n  <md-card-content>\n    <p>{{meme.description}}</p>\n  </md-card-content>\n  <md-card-actions>\n    <button md-button (click)=\"onShare()\">SHARE</button>\n    <button md-button (click)=\"onRemove()\">REMOVE</button>\n  </md-card-actions>\n</md-card>\n"

/***/ },

/***/ 760:
/***/ function(module, exports) {

module.exports = "<md-toolbar class=\"navbar md-elevation-z6\" color=\"primary\">\n  <span>Internet Meme Catalog</span>\n\n  <!-- This fills the remaining space of the current row -->\n  <span class=\"fill-space\"></span>\n\n  <md-select placeholder=\"Filter by genre\">\n      <md-option value=\"\" (onSelect)=\"showAll()\">Show All</md-option>\n      <md-option *ngFor=\"let genre of genres\" [value]=\"genre.id\" (onSelect)=\"showForGenre(genre.id)\">\n          {{genre.name}}\n      </md-option>\n  </md-select>\n\n  &nbsp;\n\n  <button md-button (click)=\"openAddDialog()\">Add Meme</button>\n</md-toolbar>\n\n<md-grid-list cols=\"5\" rowHeight=\"3:5\">\n  <md-grid-tile *ngFor=\"let meme of memes\">\n    <meme-card \n      [meme]=\"meme\"\n      [genreName]=\"getGenreName(meme)\"\n      (remove)=\"openRemoveDialog($event)\"\n      (share)=\"onShareMeme($event)\"\n    ></meme-card>\n  </md-grid-tile>\n</md-grid-list>\n"

/***/ },

/***/ 761:
/***/ function(module, exports) {

module.exports = "<h1 md-dialog-title>Remove Meme</h1>\r\n\r\n<div md-dialog-content>\r\n    <p>Really delete this meme?</p>\r\n    <img src=\"{{meme.url}}\">\r\n</div>\r\n\r\n<div md-dialog-actions>\r\n    <button type=\"button\" md-button (click)=\"cancel()\">No</button>\r\n    <button type=\"submit\" md-button (click)=\"confirm()\">Yes</button>\r\n</div>\r\n"

/***/ },

/***/ 798:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(444);


/***/ }

},[798]);
//# sourceMappingURL=main.bundle.map