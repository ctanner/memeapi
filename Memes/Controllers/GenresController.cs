﻿using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using Memes.Data.Models;
using Memes.Data.Repositories;
using System.Linq;
using System.Web.Http.Cors;

namespace Memes.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class GenresController : ApiController
    {
        private IGenreRepository _genreRepo;

        public GenresController(IGenreRepository genreRepo)
        {
            _genreRepo = genreRepo;
        }

        // GET: api/Genres
        public IQueryable<Genre> GetGenres()
        {
            return _genreRepo.GetAll();
        }

        // GET: api/Genres/5
        [ResponseType(typeof(Genre))]
        public IHttpActionResult GetGenre(int id)
        {
            Genre genre = _genreRepo.GetById(id);

            if (genre == null)
            {
                return NotFound();
            }

            return Ok(genre);
        }

        // PUT: api/Genres/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutGenre(int id, Genre genre)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != genre.Id)
            {
                return BadRequest();
            }

            if (!_genreRepo.Exists(id))
            {
                return NotFound();
            }

            _genreRepo.Update(genre);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Genres
        [ResponseType(typeof(Genre))]
        public IHttpActionResult PostGenre(Genre genre)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _genreRepo.Add(genre);

            return CreatedAtRoute("DefaultApi", new { id = genre.Id }, genre);
        }

        // DELETE: api/Genres/5
        [ResponseType(typeof(Genre))]
        public IHttpActionResult DeleteGenre(int id)
        {
            Genre genre = _genreRepo.GetById(id);

            if (genre == null)
            {
                return NotFound();
            }

            _genreRepo.Delete(genre);

            return Ok(genre);
        }
    }
}